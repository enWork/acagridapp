#简单express模板

## 使用

1. clone并生成新项目

    ```
    git clone https://git.oschina.net/react-module/eprtemp.git example
    ```

2. 下载依赖包&&启动程序

    ```
    npm install && npm start
    ```

## 要点

1. 使用`express`默认生成器，基础模板使用`jade`,所以改为使用`ejs`

    ```
    express -e example
    ```



2. 使用`nodemon`监视代码修改

    ```
    nodemon ./app/bin/www"
    ```
