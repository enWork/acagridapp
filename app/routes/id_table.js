var express = require('express');
var router = express.Router();

model = require('../models/model');

router.get('/', function(req, res, next) {

  var idItem = req.query.id;
  model.idDatas.findOne({"_id":idItem}, function(err,docs) {
    // console.log(docs.datas);
    res.render('id_table',{docs:docs.datas});
   });
});
module.exports = router;
