var express = require('express');
var router = express.Router();
model = require('../models/model')
router.get('/', function(req, res, next) {
  let response = res;
  var pages = req.query.page
    ? parseInt(req.query.page)
    : 1;
  var inp = req.query.input;
  // let aa=inp.replace(/\s*/g,'');
  let inputRemoveCharacter = inp.replace(/\s|[\~!。.；;@#$%^&*-_]/g, '');
  let input = new RegExp(inputRemoveCharacter);
  model.mapListLessNumbers.find({
    "character": input
  }, function(err, result, res) {}).limit(10).skip((pages - 1) * 10).exec(function(e, result) {
    // console.log(result);//id和特征
    var data = [];
    var datas = [];
    for (var i in result) {
      // console.log(result[i]);
      model.idDatas.findOne({
        "_id": result[i].idItem
      }, function(err, doc) {
        // console.log(doc);
        id_data = {
          id: doc._id,
          data: doc.datas
        }
        data.push(id_data);
        var title = doc.titlesId;
        datas = {
          list: data,
          title,
          pages
        };
      });
    }
    model.idDatas.find({}, function() {
      response.render('search_table', {docs: datas});
    })
  });
});

router.get('/character', function(req, res, next) {
  let response = res;
  var input=req.query.character;
  if(input===''){
    input=' ';
  }
  let inputCharacter = new RegExp(input);
  var result = [];
  model.allCharacter_distribute.find({
    'character': inputCharacter
  }, (e, docs) => {
    console.log(docs);
    if(inputCharacter==' '){
      response.render('nothing');
    }else{
      if(docs.length!==0){
        for (var i in docs) {
          // console.log(docs[i].character);
          result.push(docs[i].character);
          d = {
            list: result
          }
        }
        response.render('listCharacter', {docs: d});
      }else{
        response.render('nothing');
      }
    }
  })
});
module.exports = router;
