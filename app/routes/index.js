var router = require('express').Router(),
  model = require('../models/model');

/* GET home page. */
router.get('/', function(req, res, next) {

  // var user = new model.User({email: 'yiyern@yahoo.com', name: 'yiyern'});
  // user.save();
  //
  // var data = {
  //   title: '模板',
  //   yas: '速度解决'
  // }

  res.render('index');
});

router.get('/footer', function(req, res, next) {
  res.render('tpl/common/footer');
});
router.get('/header', function(req, res, next) {
  res.render('tpl/common/header');
});
router.get('/aside', function(req, res, next) {
  res.render('tpl/common/aside');
});
router.get('/manager', function(req, res, next) {
  res.render('manager');
});
router.get('/paper', function(req, res, next) {
  res.render('tpl/paper');
});
router.get('/book', function(req, res, next) {
  res.render('tpl/book');
});
router.get('/certicate', function(req, res, next) {
  res.render('tpl/certicate');
});
router.get('/continuingEducation', function(req, res, next) {
  res.render('tpl/continuingEducation');
});
router.get('/horResearch', function(req, res, next) {
  res.render('tpl/horResearch');
});
router.get('/patent', function(req, res, next) {
  res.render('tpl/patent');
});
router.get('/personAward', function(req, res, next) {
  res.render('tpl/personAward');
});
router.get('/report', function(req, res, next) {
  res.render('tpl/report');
});
router.get('/searchDetail', function(req, res, next) {
  res.render('tpl/searchDetail');
});
// router.get('/id_table', function(req, res, next) {
//   res.render('tpl/searchDetail');
// });
router.get('/soft', function(req, res, next) {
  res.render('tpl/soft');
});
router.get('/stuAward', function(req, res, next) {
  res.render('tpl/stuAward');
});
router.get('/verticalTopic', function(req, res, next) {
  res.render('tpl/verticalTopic');
});
module.exports = router;
