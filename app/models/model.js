var mongoose = require('../config/mongoose');

// 骨架模版
var _User = new mongoose.Schema({email: String, name: String, salt: String, password: String});
var idDatas = new mongoose.Schema({
  datas: Array,
  titlesId: Array
}, {collection: "idDatas"})
var mapListLessNumbers = new mongoose.Schema({
      character:Array,
      idItem:String,
},{collection:"mapListLessNumbers"})

var allCharacter_distribute = new mongoose.Schema({
      id:String,
      character:String,
},{collection:"allCharacter_distribute"})

module.exports = {
  idDatas: mongoose.model('idDatas', idDatas),
  mapListLessNumbers: mongoose.model('mapListLessNumbers', mapListLessNumbers),
  allCharacter_distribute: mongoose.model('allCharacter_distribute', allCharacter_distribute)

};
