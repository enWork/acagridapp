var select = function(ele) {
  $lis = $(ele).find('li');
  for (var i = 0; i < $lis.length; i++) {
    entity($lis[i]);
  }
}

var entity = function(eleli) {
  var $that = $(eleli);
  var $ul = $that.parent('.select_list');
  var $ipt = $ul.siblings('.select_input');
  $ipt.on('focus', function() {
    $ul.css('display', 'block');
  });
  $ipt.on('blur', function() {
    setTimeout(function() {
      $ul.css('display', 'none');
    }, 200)
  });

  $that.on('click', function() {
    $ipt.attr('placeholder','');
    $ipt.val($that.children('a').html());
  });
}
