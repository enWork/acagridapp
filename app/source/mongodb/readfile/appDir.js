var MongoClient = require("mongodb").MongoClient;
var DB_URL = "mongodb://localhost:27017/test";

var fs = require('fs');
function strMapToObj(strMap) {
  let obj = Object.create(null);
  for (let [k,
    v]of strMap) {
    obj[k] = v;
  }
  return obj;
}

function strMapToJson(strMap) {
  return JSON.stringify(strMapToObj(strMap));
}
var DataList = [];
var Datas;
fs.readdir("/Users/eh/Documents/mywork/htm1/three", function(err, files) {
  if (err) {
    // console.log(err);
    return;
  }
  console.log(files.length);
  files.forEach(function(filename) {
    var myMap = new Map();
    var readline = require('linebyline'),
      rl = readline('/Users/eh/Documents/mywork/htm1/three/' + filename);
    var temp1;
    rl.on('line', function(line, lineCount, byteCount) {
      var regex = /^【(.*)】/; //有【】
      var regex1 = /^[^【]|^$/;
      var regex2 = /^[^【】$]/; //没有中括号
      var regex3 = /^$/; //空白行
      if (line.match(regex)) { //匹配【】
        // console.log(line);
        temp1 = line.match(regex).input.toString();
        myMap.set(temp1, '');
      } else if (line.match(regex1)) {
        if (line.match(regex2)) { //有值
          var value1 = myMap.get(temp1);
          var value = line.match(regex2).input;
          myMap.set(temp1, value1 + value);
        }
        if (line.match(regex3)) { //空白行
          var valueBlank1 = myMap.get(temp1);
          var valueBlank = line.match(regex3).input;
          myMap.set(temp1, valueBlank1 + valueBlank);
        }
      }
    }).on('close', function() {
      Datas = strMapToObj(myMap);
      DataList.push(Datas);
      //  fs.writeFile("./data.json", DataList, (err) => {})
      // console.log(Datas);
      MongoClient.connect(DB_URL, function(error, db) {
        // console.log(Datas);
        // console.log('连接成功!');
        insertData(db);
      });

    });

  });
});

function insertData(db) {
  var devices = db.collection('col-test');
  devices.insert(DataList, function(error, result) {
    if (error) {
      console.log('Error:' + error);
    } else {
      // console.log(result.result.n);
    }
    db.close();
  });
}
