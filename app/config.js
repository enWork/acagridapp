var mongodb = {
  url: 'mongodb://127.0.0.1:27017/tesinfo',
  opts: {
    useMongoClient: true
  }
}

module.exports = {
  mongodb: mongodb
};
