var mongoose = require('mongoose'),
  config_mongodb = require('../config')['mongodb'];

mongoose.connect('mongodb://127.0.0.1:27017/endocrineInfo');

module.exports = mongoose;
